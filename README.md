# README #

Tiny application to test I/O things via serial(/or usb) interface in Arduino/Maple mini projects. At the moment Arduino Uno/Mini, Leonardo/Micro and Maple Mini are added, but I haven't tested these completely. At the default all pins are as input, but can be changed to output, pwm or analog input via serial port. Status of input pins can be monitored.

Warning. Setting I/O-pin that is connected to UP/DOWN without sufficient resistor as OUTPUT, will fry your microcontroller!

### What is this repository for? ###
* Monitor and control I/O-pins via Serial Port
* Uses primary serial port:
    * Serial in Uno, Mini Pro   
    * USB in micro, leonardo, Maple Mini
* Arduino IDE 1.6.6
    * [Arduino_STM32 for Maple Mini](https://github.com/rogerclarkmelbourne/Arduino_STM32/wiki/Installation) 

### Serial Port Menu: ###
* m...................monitoring mode
* sm..................show pin modes
* si..................show input states
* st..................show time
* ri..................reset all io to inputs
* rt[HH:MM:SS.SSS]....reset time(r)
* pmo[0-33]...........pin mode output
* pmi[0-33]...........pin mode input
* pmp[0-33]...........pin mode pwm
* pma[03-11]..........pin mode analog
* poh[0-33]...........pin output high
* pol[0-33]...........pin output low
* pop[0-33],[0-4095]..pin output pwm

### The MIT License (MIT) ###

Copyright (c) 2016 kaprot

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.