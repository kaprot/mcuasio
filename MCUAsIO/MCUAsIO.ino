#define PORT_FORMAT "%02X" //8bit DEFAULT
#define ANALOG_PORT_FORMAT "%03X" //12bit DEFAULT
#define ANALOGINPUT INPUT
#define PWMOUTPUT OUTPUT
#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
      #define BOARD "ATMEGA328P"
      #define BREG PINB
      #define CREG PINC
      #define DREG PIND
      #define DPIN_COUNT 20 
      #define APIN_COUNT 6
      #define FIRST_APIN 14
      #define ANALOG_MASK 0b11111100000000000000
      #define PWM_MASK 0b111001101000
      #define BITS 8
      #define PWM_BITS 8
      #define BAUDRATE 19200
#elif defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
 
    //Code here

#elif defined(__AVR_ATmega32U4__)
      #define BOARD "Micro/Leonardo"
      #define BREG PINB
      #define CREG PINC
      #define DREG PIND
      #define EREG PINE
      #define FREG PINF
      #define DPIN_COUNT 20
      #define APIN_COUNT 6
      #define FIRST_APIN 14
      #define BITS 8
      #define ANALOG_MASK 0b11111100000000000000
      #define PWM_MASK 0b1110000000000000111100111000
      #define PWM_BITS 8
      #define BAUDRATE 115200

#elif defined(__STM32F1__)
      #define BOARD "MAPLE"
      #define AREG GPIOA->regs->IDR
      #define BREG GPIOB->regs->IDR
      #define CREG GPIOC->regs->IDR
      #define DPIN_COUNT 34
      #define APIN_COUNT 9
      #define FIRST_APIN 3    
      #define PORT_FORMAT "%04X" //16bit
      #define ANALOG_PORT_FORMAT "%04X" //16bit
      #define BITS 16
      #define PWM_BITS 12
      #define BAUDRATE
      #define ANALOG_MASK 0b111111111000
      #define PWM_MASK 0b11100110100
      #define PWMOUTPUT PWM
      #define ANALOGINPUT INPUT_ANALOG
#else
      define BOARD "NONE"
#endif
#define MAXSTRINGLEN 255
// Variables:
char cmd[MAXSTRINGLEN];// a string to hold incoming data
int inputCounter=0;
byte specialPins[DPIN_COUNT];
boolean stringComplete = false;  // whether the string is complete
int mode=0;
unsigned long currentTime=0;

void setup() {
    Serial.begin(BAUDRATE);
    for (int c=0;c<DPIN_COUNT;c++)specialPins[c]=0;
}
void serialEvent(){
  while (Serial.available()) {
        //Serial.print(millis());
        char inChar = (char)Serial.read();
        cmd[inputCounter] = inChar;
        Serial.print(inChar);
        inputCounter++;
        if (inputCounter>=MAXSTRINGLEN){
          inputCounter=0;
          Serial.println("NOK: Too long command.");
        }
        if (inChar == '\r') {
            stringComplete = true;
            inputCounter=0;
        }
      }
}
void printTime(bool startNewLine=false){
  if(startNewLine==true)Serial.println();
  char buf[50];
  unsigned long ct=(millis()-currentTime);
  unsigned long over;
  sprintf(buf, "%02d",int(ct/3600000)%24);
  Serial.print(buf);
  over=ct%3600000;unsigned long currentTime=0;
  sprintf(buf, ":%02d", int(over/60000));
  Serial.print(buf);
  over=over%60000;
  sprintf(buf, ":%02d", int(over/1000));
  Serial.print(buf);
  over=over%1000;
  sprintf(buf, ".%03d ", int(over%1000));
  Serial.print(buf);
}
void printHelp(){
  char buffer[50];
  Serial.println("MCUAsIO");
  Serial.print(BOARD);
  Serial.print(" BUILD:");
  Serial.print(__DATE__);
  Serial.print(" ");
  Serial.println(__TIME__);
  Serial.println("m...................monitoring mode");
  Serial.println("sm..................show pin modes");
  Serial.println("si..................show input states");
  Serial.println("st..................show time");
  Serial.println("ri..................reset all io to inputs");
  Serial.println("rt[HH:MM:SS.SSS]....reset time(r)");
  sprintf(buffer,"pmo[0-%02d]...........pin mode output",DPIN_COUNT-1);Serial.println(buffer);
  sprintf(buffer,"pmi[0-%02d]...........pin mode input",DPIN_COUNT-1);Serial.println(buffer);
  sprintf(buffer,"pmp[0-%02d]...........pin mode pwm",DPIN_COUNT-1);Serial.println(buffer);
  sprintf(buffer,"pma[%02d-%02d]..........pin mode analog",FIRST_APIN,FIRST_APIN+APIN_COUNT-1);Serial.println(buffer);
  sprintf(buffer,"poh[0-%02d]...........pin output high",DPIN_COUNT-1);Serial.println(buffer);
  sprintf(buffer,"pol[0-%02d]...........pin output low",DPIN_COUNT-1);Serial.println(buffer);
  sprintf(buffer,"pop[0-%02d],[0-%04d]..pin output pwm",DPIN_COUNT-1,(1<<PWM_BITS)-1);Serial.println(buffer);
}

void printRegs(int mode){
  char buffer[20];
    //sprintf(buffer,"%lu:",millis());Serial.print(buffer);
    printTime(false);
  if (mode==1){
    #ifdef AREG
      sprintf(buffer,PORT_FORMAT,AREG);Serial.print(buffer);
    #endif
    #ifdef BREG
      sprintf(buffer,PORT_FORMAT,BREG);Serial.print(buffer);
    #endif
    #ifdef CREG
      sprintf(buffer,PORT_FORMAT,CREG);Serial.print(buffer);
    #endif
    #ifdef DREG
      sprintf(buffer,PORT_FORMAT,DREG);Serial.print(buffer);
    #endif
    #ifdef EREG
      sprintf(buffer,PORT_FORMAT,EREG);Serial.print(buffer);
    #endif
    #ifdef FREG
      sprintf(buffer,PORT_FORMAT,FREG);Serial.print(buffer);
    #endif
  }
  for (int c=0;c<DPIN_COUNT;c++)
    {
    if (specialPins[c]==0 && mode==2)
      {
      //Serial.print("-");
      Serial.print(digitalRead(c));  
      }
    
    if (specialPins[c]==ANALOGINPUT+2)
      {
      Serial.print("-");
      sprintf(buffer,ANALOG_PORT_FORMAT,analogRead(c));Serial.print(buffer);  
      }
    }
Serial.println();
}

void printModes(){
  char buffer[50];
  Serial.print("Pin ");
  for (int c=0;c<DPIN_COUNT;c++)
    {
    sprintf(buffer,"%02d ",c);Serial.print(buffer);
    }  
  Serial.println();
  Serial.print("Dig ");
  for (int c=0;c<DPIN_COUNT;c++)
    {
    //sprintf(buffer,"%02d ",(*portModeRegister(digitalPinToPort(c)) & digitalPinToBitMask(c))!=0);Serial.print(buffer);
    sprintf(buffer," %d ",specialPins[c]==1);Serial.print(buffer);
    }  
  Serial.println();
  Serial.print("Ana ");
  for (int c=0;c<DPIN_COUNT;c++)
    {
    sprintf(buffer,"%d%d ",(ANALOG_MASK & 1UL<<c)!=0,specialPins[c]==ANALOGINPUT+2);Serial.print(buffer);
    }
  Serial.println();
  Serial.print("Pwm ");
  for (int c=0;c<DPIN_COUNT;c++)
    {
    sprintf(buffer,"%d%d ",(PWM_MASK & 1UL<<c)!=0,specialPins[c]==PWMOUTPUT+2);Serial.print(buffer);
    }  
  Serial.println();
}
void printCmdStatus(char* msg,boolean *stat, int nbr=-999,boolean status=true){
  if (strlen(msg))Serial.print(msg);
  if (nbr!=-999)Serial.print(nbr);
  if (strlen(msg))Serial.println();
  if (status==false)
  {
    Serial.print("Nok");
  }
  else 
  {
    Serial.println("Ok");
    *stat=true;
  }
}
void handlecmd(){
      int pin=-1;
      int value=-1;
      boolean stat=false;
      Serial.println();
      //char buffer[50];
      if (cmd[0]=='\r'){
        printCmdStatus("",&stat);
      }
      if (cmd[0]=='?' && cmd[1]=='\r'){
        printHelp();
        printCmdStatus("",&stat);
      }
      if (cmd[0]=='m' && cmd[1]=='\r'){
        printCmdStatus("Mode set to ",&stat,mode=(mode+1)%3);
      }
      if (cmd[0]=='s' && cmd[1]=='t'&& cmd[2]=='\r'){
        printTime(false);
        printCmdStatus(" current time",&stat);
      }
      if (cmd[0]=='s' && cmd[1]=='m' && cmd[2]=='\r'){
        printModes();
        printCmdStatus("",&stat);
      }
      if (cmd[0]=='s' && cmd[1]=='i' && cmd[2]=='\r'){
        printRegs(1);
        printCmdStatus("",&stat);
      }
      if (cmd[0]=='r' && cmd[1]=='i' && cmd[2]=='\r'){
        for (int c=0;c<DPIN_COUNT;c++)
          {
          specialPins[c]=0;
          pinMode(c, INPUT);
          }
        printCmdStatus("IOs resetted to INPUT",&stat);
      }
       if (cmd[0]=='r' && cmd[1]=='t'){
       unsigned int hours,mins,secs,hsecs;
          char tmp[50];
          strncpy(tmp,cmd,49);
          if (cmd[2]=='\r'){
            currentTime=millis();
            printCmdStatus("Timer reseted",&stat);
            }
          else if(sscanf(tmp,"rt%d:%d:%d.%d",&hours,&mins,&secs,&hsecs)){
            currentTime=millis()-((3600000*hours)+(60000*mins)+(1000*secs)+hsecs);
            printTime();
            printCmdStatus("Time has been set",&stat);
            }
      }
      if (sscanf(cmd,"pmo%d",&pin) && pin<DPIN_COUNT){
        specialPins[pin]=1;
        pinMode(pin, OUTPUT);
        printCmdStatus("OUTPUT mode set to PIN ",&stat,pin);
      }
      if (sscanf(cmd,"pmi%d",&pin) && pin<DPIN_COUNT){
        specialPins[pin]=0;
        pinMode(pin, INPUT);
        printCmdStatus("INPUT mode set to PIN ",&stat,pin);
      }
      if (sscanf(cmd,"pma%d",&pin) && pin<DPIN_COUNT && (ANALOG_MASK & 1UL<<pin)){
        //if (ANALOG_MASK & 1<<pin)AnalogPins=AnalogPins&~1<<(pin-FIRST_APIN);
        pinMode(pin, ANALOGINPUT);
        specialPins[pin]=ANALOGINPUT+2;
        printCmdStatus("ANALOG mode set to PIN ",&stat,pin);
      }
       if (sscanf(cmd,"pmp%d",&pin) && pin<DPIN_COUNT){
        if (PWM_MASK & 1<<pin){
          pinMode(pin,PWMOUTPUT);
          specialPins[pin]=PWMOUTPUT+2;
          printCmdStatus("PWM mode set to PIN ",&stat,pin);
        }
      }
      if (sscanf(cmd,"pol%d",&pin) && pin<DPIN_COUNT){
        digitalWrite(pin, LOW); 
        printCmdStatus("LOW PIN:",&stat,pin);
      }
      if (sscanf(cmd,"poh%d",&pin) && pin<DPIN_COUNT){
        digitalWrite(pin, HIGH); 
        printCmdStatus("HIGH PIN:",&stat,pin);
      }
      if (sscanf(cmd,"pop%d,%d",&pin,&value)==2 && pin<DPIN_COUNT && specialPins[pin]==PWMOUTPUT+2){
        analogWrite(pin, value); 
        Serial.print(value);
        printCmdStatus(" PWM PIN:",&stat,pin);
      }
      if (stat==false)printCmdStatus("Error!",false,-999,false);
      stringComplete = false;
      inputCounter=0;
      Serial.print ("> ");
      char bs =8;
      Serial.print(bs); 
}

void loop() {
    if (stringComplete == true)handlecmd();
    #if defined(__STM32F1__) || defined(__AVR_ATmega32U4__)
      serialEvent();
    #endif
      if (mode!=0){
        printRegs(mode);
      }
}


